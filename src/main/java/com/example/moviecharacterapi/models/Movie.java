package com.example.moviecharacterapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Movie model class, responsible for movie objects as well as
 * creating the movie table with its attributes
 * and defining the relation with different tables
 *
 */
@Entity
public class Movie{

    // auto increment pk
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long movieId;

    @Column(name = "title")
    private String title;

    @Column(name = "year")
    private String year;

    @Column(name = "image")
    private String image;

    @Column(name = "trailer")
    private String trailer;

    // many to many relation with role with a composite pk
    @ManyToMany
    @JoinTable(name = "movie_Has_Role",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private Set<Role> roles;
    /**
     * @return a list of roles for each movie in the form of urls
     */
    @JsonGetter("roles")
    public Set<String> getJsonRoles() {
        if (roles != null)
            return roles.stream().map(role -> "/api/v1/roles/" + role.getRoleId()).collect(Collectors.toSet());
        return null;
    }


    @ManyToMany
    @JoinTable(name = "movie_Has_Director",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "director_id")}
    )
    private Set<Director> directors;

    /**
     * @return a list of directors for each movie in the form of urls
     */
    @JsonGetter("directors")
    public Set<String> getJsonDirectors() {
        if (directors != null)
            return directors.stream().map(director -> "/api/v1/directors/" + director.getDirectorId()).collect(Collectors.toSet());
        return null;
    }

    @ManyToMany
    @JoinTable(name = "movie_Has_Genre",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")}
    )
    private Set<Genre> genres;
    /**
     * @return a list of genres for each movie in the form of urls
     */
    @JsonGetter("genres")
    public Set<String> getJsonGenres() {
        if (genres != null)
            return genres.stream().map(genre -> "/api/v1/genres/" + genre.getGenreId()).collect(Collectors.toSet());
        return null;
    }

    @ManyToOne()
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    /**
     * @return a franchise for each movie in the form of urls
     */
    @JsonGetter("franchise")
    public String getJsonAuthor() {
        if (franchise != null)
            return "/api/v1/franchises/" + franchise.getFranchiseId();
        return null;
    }


    public Movie() {
    }

    public Movie(long movieId, String title, String image, String trailer, Set<Role> roles, Set<Director> directors, Set<Genre> genres, Franchise franchise, String year) {
        this.movieId = movieId;
        this.title = title;
        this.image = image;
        this.trailer = trailer;
        this.roles = roles;
        this.directors = directors;
        this.genres = genres;
        this.franchise = franchise;
        this.year = year;
    }

    // Getters and Setters


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(Set<Director> directors) {
        this.directors = directors;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }


}
