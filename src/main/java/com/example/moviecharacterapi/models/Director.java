package com.example.moviecharacterapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Director model class, responsible for director objects as well as
 * creating the director table with its attributes
 * and defining the relation with different tables
 *
 */
@Entity
public class Director {
    // auto increment pk
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long directorId;

    @Column(name = "name")
    private String name;

    // many to many relation with movie with a composite pk
    @ManyToMany
    @JoinTable(name = "movie_Has_Director",
            joinColumns = {@JoinColumn(name = "director_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> movies;

    /**
     * @return a list of movies for each director in the form of urls
     */
    @JsonGetter("movies")
    public Set<String> getJsonMovies() {
        if (movies != null)
            return movies.stream().map(movie -> "/api/v1/movies/" + movie.getMovieId()).collect(Collectors.toSet());
        return null;
    }

    public Director() {
    }

    public Director(long directorId, String name, Set<Movie> movies) {
        this.directorId = directorId;
        this.name = name;
        this.movies = movies;
    }

    public long getDirectorId() {
        return directorId;
    }

    public void setDirectorId(long directorId) {
        this.directorId = directorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
