package com.example.moviecharacterapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * Role model class, responsible for role objects as well as
 * creating the role table with its attributes
 * and defining the relation with different tables
 *
 */
@Entity
public class Role {

    // auto increment pk
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long roleId;

    @Column(name = "name")
    private String name;

    @Column(name = "alias")
    private String alias;

    @Column(name = "gender")
    private String gender;

    @Column(name = "picture")
    private String picture;

    // many to many relation with the movie with a composite pk
    @ManyToMany
    @JoinTable(name = "movie_Has_Role",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )

    private Set<Movie> movies;
    /**
     * @return a list of movies for each role in the form of urls
     */
    @JsonGetter("movies")
    public Set<String> getJsonMovies() {
        if (movies != null)
            return movies.stream().map(movie -> "/api/v1/movies/" + movie.getMovieId()).collect(Collectors.toSet());
        return null;
    }

    // Constructor

    public Role() {
    }

    public Role(long roleId, String name, String alias, String gender, String picture, Set<Movie> movies) {
        this.roleId = roleId;
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movies = movies;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
