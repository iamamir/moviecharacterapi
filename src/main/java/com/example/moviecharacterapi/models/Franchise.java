package com.example.moviecharacterapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Franchise model class, responsible for franchise objects as well as
 * creating the franchise table with its attributes
 * and defining the relation with different tables
 *
 */
@Entity
public class Franchise {

    // auto increment pk
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long franchiseId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;

    /**
     * @return a list of movies for each franchise in the form of urls
     */
    @JsonGetter("movies")
    public Set<String> getJsonMovies(){
        if (movies != null)
            return movies.stream().map(movie -> "/api/v1/movies/"+movie.getMovieId()).collect(Collectors.toSet());
        return null;
    }


    public Franchise() {
    }

    public Franchise(long franchiseId, String name, String description, Set<Movie> movies) {
        this.franchiseId = franchiseId;
        this.name = name;
        this.description = description;
        this.movies = movies;
    }

    public long getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(long franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
