package com.example.moviecharacterapi.services;

import com.example.moviecharacterapi.models.Franchise;
import com.example.moviecharacterapi.models.Movie;
import com.example.moviecharacterapi.models.Role;
import com.example.moviecharacterapi.repositories.FranchiseRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Service class, to handle the franchise CRUD operations
 */
public class FranchiseService {

    // instance of the franchise repo
    private final FranchiseRepository franchiseRepository;

    public FranchiseService(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    /**
     * to fetch all the franchises from the db
     *
     * @return a list of franchises if the list is not empty as well as Ok httpstatus
     * no content and null is returned if the list is empty
     */
    public ResponseEntity<List<Franchise>> getAllFranchises() {

        List<Franchise> franchises = franchiseRepository.findAll();
        if (franchises.size() > 0) {
            return new ResponseEntity<>(franchises, HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to fetch a specific franchise from the db
     *
     * @param id the id of the franchise
     * @return if it exists a franchise is returned with OK httpstatus
     * if it does not exist, null and no content httpstatus is returned
     */
    public ResponseEntity<Franchise> getFranchise(long id) {
        if (franchiseRepository.existsById(id)) {
            Franchise franchise = franchiseRepository.findById(id).get();
            return new ResponseEntity<>(franchise, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to add a new franchise to the db
     *
     * @param franchise the to be added franchise
     * @return a franchise object if it is added successfully, as well as created httpstatus
     * if the user defined the id of the franchise, then it won't be created and not acceptable is returned
     */
    public ResponseEntity<Franchise> addFranchise(Franchise franchise) {
        if (franchise.getFranchiseId() == 0) {
            franchiseRepository.save(franchise);
            return new ResponseEntity<>(franchise, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(franchise, HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * to update a specific franchise
     *
     * @param inputId   path variable id
     * @param franchise the updated version of the franchise
     * @return if it's successful, an updated version of the franchise is returned as well as Httpstatus ok
     */
    public ResponseEntity<Franchise> updateFranchise(long inputId, Franchise franchise) {

        long id = franchise.getFranchiseId();

        // checks if the franchise doesn't exist
        if (!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>(franchise, HttpStatus.NO_CONTENT);
        }

        // if the path variable doesn't equal the franchise id
        if (id != inputId) {
            return new ResponseEntity<>(franchise, HttpStatus.BAD_REQUEST);
        }

        // an instance of the franchise is fetched
        Franchise outputFranchise = franchiseRepository.findById(id).get();


        // the coming checks is to check which fields are to be updated
        // if the franchise has null values for some fields, it wont be updated.

        if (franchise.getName() != null) {
            outputFranchise.setName(franchise.getName());
        }

        if (franchise.getDescription() != null) {
            outputFranchise.setDescription(franchise.getDescription());
        }

        if (franchise.getMovies() != null) {
            outputFranchise.setMovies(franchise.getMovies());
        }

        franchiseRepository.save(outputFranchise);

        return new ResponseEntity<>(outputFranchise, HttpStatus.OK);
    }

    /**
     * to delete a franchise from the db without removing the related values from other tables
     *
     * @param id the id of the to be removed franchise
     * @return franchise object of the removed franchise and ok httpstatus
     */
    public ResponseEntity<Franchise> deleteFranchise(long id) {
        //checks if it exists
        if (franchiseRepository.existsById(id)) {
            Franchise franchise = franchiseRepository.findById(id).get();
            // fetches the movies of that franchise and set the franchise to be null
            Set<Movie> movies = franchise.getMovies();

            for (Movie movie : movies) {
                movie.setFranchise(null);
            }
            //also sets the list of movies to be null for that franchise
            franchise.setMovies(null);

            //removes the franchise
            franchiseRepository.delete(franchise);
            return new ResponseEntity<>(franchise, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to fetch a list of movies for a specific franchise
     *
     * @param id the franchise id
     * @return the list of movies as well as httpstatus ok
     */
    public ResponseEntity<Set<Movie>> getMoviesInFranchise(long id) {

        // checks if the franchise exists
        if (franchiseRepository.existsById(id)) {
            Franchise franchise = franchiseRepository.findById(id).get();
            Set<Movie> movies = franchise.getMovies();
            return new ResponseEntity(movies, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to fetch all roles in a specific franchise
     * @param id the franchise id
     * @return a list of roles(characters) as well as httpstatus ok if successful
     */
    public ResponseEntity<List<Role>> getRolesInFranchise(long id) {

       //checks if it exists
        if (franchiseRepository.existsById(id)) {
            // in that case it fetchs the franchise,
            // then the movies and the list of roles for each movie
            // and add them to the list of roles, and returns the roles
            Franchise franchise = franchiseRepository.findById(id).get();
            Set<Movie> movies = franchise.getMovies();
            List<Role> roles = new ArrayList<>();
            for (Movie movie : movies) {
                Set<Role> rolesInMovie = movie.getRoles();
                for (Role role : rolesInMovie) {
                    if (!roles.contains(role)) {
                        roles.add(role);
                    }
                }
            }
            return new ResponseEntity(roles, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }


}
