package com.example.moviecharacterapi.services;

import com.example.moviecharacterapi.models.Genre;
import com.example.moviecharacterapi.models.Movie;
import com.example.moviecharacterapi.repositories.GenreRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.List;
import java.util.Set;

/**
 * Service class, to handle the genre CRUD operations
 */
public class GenreService {
    // instance of the genre repo
    private final GenreRepository genreRepository;

    public GenreService(GenreRepository genreRepository){
        this.genreRepository = genreRepository;
    }

    /**
     * to fetch all the genres from the db
     * @return a list of genres if the list is not empty as well as Ok httpstatus
     * no content and null is returned if the list is empty
     */
    public ResponseEntity<List<Genre>> getAllGenres(){
        List<Genre> genres = genreRepository.findAll();
        if(genres.size() > 0){
            return new ResponseEntity<>(genres, HttpStatus.OK);
        }
        return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
    }

    /**
     * to fetch a specific genre from the db
     * @param id the id of the genre
     * @return if it exists a genre is returned with OK httpstatus
     *  if it does not exist, null and no content httpstatus is returned
     */
    public ResponseEntity<Genre> getGenre(long id){
        if (genreRepository.existsById(id)) {
            Genre genre = genreRepository.findById(id).get();
            return new ResponseEntity<>(genre, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to add a new genre to the db
     * @param genre the to be added genre
     * @return a genre object if it is added successfully, as well as created httpstatus
     * if the user defined the id of the genre, then it won't be created and not acceptable is returned
     */
    public ResponseEntity<Genre> addGenre(Genre genre){
        // the genre cannot have an id! as it's created automatically with hibernate
        if(genre.getGenreId()  == 0){
            genreRepository.save(genre);
            return new ResponseEntity<>(genre,HttpStatus.CREATED);
        }
        return new ResponseEntity<>(genre, HttpStatus.NOT_ACCEPTABLE);
    }


    /**
     * to update a specific genre
     * @param inputId path variable id
     * @param genre the updated version of the genre
     * @return if it's successful, an updated version of the genre is returned as well as Httpstatus ok
     *
     */
    public ResponseEntity<Genre> updateGenre(long inputId, Genre genre){

        long id = genre.getGenreId();

        // checks if the genre doesn't exist
        if(!genreRepository.existsById(id)){
            return new ResponseEntity<>(genre, HttpStatus.NO_CONTENT);
        }
        // if the path variable doesn't equal the genre id
        if(id != inputId){
            return new ResponseEntity<>(genre, HttpStatus.BAD_REQUEST);
        }

        // an instance of the genre is fetched
        Genre outputGenre = genreRepository.findById(id).get();

        // the coming checks is to check which fields are to be updated
        // if the genre has null values for some fields, it wont be updated.

        if(genre.getType() != null){
            outputGenre.setType(genre.getType());
        }

        if(genre.getMovies() != null){
            outputGenre.setMovies(genre.getMovies());
        }

        genreRepository.save(outputGenre);

        return new ResponseEntity<>(outputGenre,HttpStatus.OK);
    }

    /**
     * to delete a genre from the db without removing the related values from other tables
     * @param id the id of the to be removed genre
     * @return genre object of the removed genre and ok httpstatus
     */
    public ResponseEntity<Genre> deleteGenre(long id){

        // checks if it exists
        if(genreRepository.existsById(id)){
            Genre genre = genreRepository.findById(id).get();
            //fetches the list of movie which has this genre,
            // then remove the genre from the movies
            Set<Movie> movies = genre.getMovies();

            for(Movie movie : movies){
                movie.getGenres().remove(movie);
            }
            genre.setMovies(null);

            genreRepository.delete(genre);

            return new ResponseEntity<>(genre,HttpStatus.OK);
        }
        return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
    }
}
