package com.example.moviecharacterapi.services;

import com.example.moviecharacterapi.models.Director;
import com.example.moviecharacterapi.models.Genre;
import com.example.moviecharacterapi.models.Movie;
import com.example.moviecharacterapi.models.Role;
import com.example.moviecharacterapi.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Service class, to handle the movie CRUD operations
 */
public class MovieService {

    // instance of the movie repo
    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    /**
     * to fetch all the movies from the db
     * @return a list of movies if the list is not empty as well as Ok httpstatus
     * no content and null is returned if the list is empty
     */
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        if (movies.size() > 0) {
            return new ResponseEntity<>(movies, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to fetch a specific movie from the db
     * @param id the id of the movie
     * @return if it exists a movie is returned with OK httpstatus
     *  if it does not exist, null and no content httpstatus is returned
     */
    public ResponseEntity<Movie> getMovie(long id) {
        if (movieRepository.existsById(id)) {
            Movie movie = movieRepository.findById(id).get();
            return new ResponseEntity<>(movie, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to add a new movie to the db
     * @param movie the to be added movie
     * @return a movie object if it is added successfully, as well as created httpstatus
     * if the user defined the id of the movie, then it won't be created and not acceptable is returned
     */
    public ResponseEntity<Movie> addMovie(Movie movie) {

        if (movie.getMovieId() == 0) {
            movieRepository.save(movie);
            return new ResponseEntity<>(movie, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * to update a specific movie
     * @param id path variable id
     * @param updateMovie the updated version of the movie
     * @return if it's successful, an updated version of the movie is returned as well as Httpstatus ok
     *
     */
    public ResponseEntity<Movie> updateMovie(long id, Movie updateMovie) {

        // checks if the movie exists
        if (movieRepository.existsById(id)) {
            // if the path variable equal the movie id
            if (id == updateMovie.getMovieId()) {

                // an instance of the movie is fetched
                Movie movie = movieRepository.findById(id).get();

                // the coming checks is to check which fields are to be updated
                // if the movie has null values for some fields, it wont be updated.

                if (updateMovie.getTitle() != null)
                    movie.setTitle(updateMovie.getTitle());

                if (updateMovie.getYear() != null)
                    movie.setYear(updateMovie.getYear());

                if (updateMovie.getImage() != null)
                    movie.setImage(updateMovie.getImage());

                if (updateMovie.getTrailer() != null)
                    movie.setTrailer(updateMovie.getTrailer());

                if (updateMovie.getRoles() != null && !updateMovie.getRoles().isEmpty()) {
                    movie.setRoles(updateMovie.getRoles());
                }
                if (updateMovie.getDirectors() != null && !updateMovie.getDirectors().isEmpty()) {
                    movie.setDirectors(updateMovie.getDirectors());
                }
                if (updateMovie.getGenres() != null && !updateMovie.getGenres().isEmpty()) {
                    movie.setGenres(updateMovie.getGenres());
                }
                if (updateMovie.getFranchise() != null) {
                    movie.setFranchise(updateMovie.getFranchise());
                }

                movieRepository.save(movie);

                return new ResponseEntity<>(updateMovie, HttpStatus.OK);
            }

            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to delete a movie from the db without removing the related values from other tables
     * @param id the id of the to be removed movie
     * @return movie object of the removed movie and ok httpstatus
     */
    public ResponseEntity<Movie> deleteMovie(long id) {
        // checks if it exists
        if (movieRepository.existsById(id)) {
            Movie movie = movieRepository.findById(id).get();

            // to remove that specific movie from the list of movies for each role
            Set<Role> roles = movie.getRoles();
            for (Role role : roles) {
                role.getMovies().remove(movie);
            }

            // to remove that specific movie from the list of movies for each genre
            Set<Genre> genres = movie.getGenres();
            for (Genre genre : genres) {
                genre.getMovies().remove(movie);
            }

            // to remove that specific movie from the list of movies for each director
            Set<Director> directors = movie.getDirectors();
            for (Director director : directors) {
                director.getMovies().remove(movie);
            }

            // to remove that specific movie from the franchise
            if (movie.getFranchise() != null) {
                movie.getFranchise().getMovies().remove(movie);
            }

            movieRepository.delete(movie);
            return new ResponseEntity<>(movie, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    /**
     * to fetch all the characters for specific movie
     * @param id movie id
     * @return returns the list of roles in a specific movie if successful (not empty) as well as httpstatus ok
     */
    public ResponseEntity<List<Role>> getRolesByMovie(long id) {
        List<Role> roles = null;
        // checks if the movie exist
        if (movieRepository.existsById(id)) {
            //fetches the roles
            roles = new ArrayList<>(movieRepository.getOne(id).getRoles());
            // checks if the list is not empty
            if (roles.size() > 0) {
                return new ResponseEntity<>(roles, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    }
}
