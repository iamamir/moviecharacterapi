package com.example.moviecharacterapi.services;

import com.example.moviecharacterapi.models.Director;
import com.example.moviecharacterapi.models.Movie;
import com.example.moviecharacterapi.repositories.DirectorRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.List;
import java.util.Set;

/**
 * Service class, to handle the director CRUD operations
 */
public class DirectorService {

    // instance of the director repo
    private final DirectorRepository directorRepository;

    public DirectorService(DirectorRepository directorRepository) {

        this.directorRepository = directorRepository;
    }

    /**
     * to fetch all the directors from the db
     * @return a list of directors if the list is not empty as well as Ok httpstatus
     * no content and null is returned if the list is empty
     */
    public ResponseEntity<List<Director>> getAllDirectors(){

        List<Director> directors = directorRepository.findAll();
        if(directors.size() > 0){
            return new ResponseEntity<>(directors,HttpStatus.OK);
        }
        return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
    }

    /**
     * to fetch a specific director from the db
     * @param id the id of the director
     * @return if it exists a director is returned with OK httpstatus
     *  if it does not exist, null and no content httpstatus is returned
     */
    public ResponseEntity<Director> getDirector(long id){
        if (directorRepository.existsById(id)) {
            Director director = directorRepository.findById(id).get();
            return new ResponseEntity<>(director, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to add a new director to the db
     * @param director the to be added director
     * @return a director object if it is added successfully, as well as created httpstatus
     * if the user defined the id of the director, then it won't be created and not acceptable is returned
     */
    public ResponseEntity<Director> addDirector(Director director){
        // the director cannot have an id!
        if(director.getDirectorId()  == 0){
            directorRepository.save(director);
            return new ResponseEntity<>(director,HttpStatus.CREATED);
        }
        return new ResponseEntity<>(director, HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * to update a specific director
     * @param inputId path variable id
     * @param director the updated version of the director
     * @return if it's successful, an updated version of the director is returned as well as Httpstatus ok
     *
     */
    public ResponseEntity<Director> updateDirector(long inputId, Director director){

        long id = director.getDirectorId();

        // checks if the director doesn't exist
        if(!directorRepository.existsById(id)){
            return new ResponseEntity<>(director, HttpStatus.NO_CONTENT);
        }

        // if the path variable doesn't equal the director id
        if(id != inputId){
            return new ResponseEntity<>(director, HttpStatus.BAD_REQUEST);
        }

        // an instance of the director is fetched
        Director outputDirector = directorRepository.findById(id).get();

        // the coming checks is to check which fields are to be updated
        // if the director has null values for some fields, it wont be updated.

        if(director.getName() != null){
            outputDirector.setName(director.getName());
        }

        if(director.getMovies() != null){
            outputDirector.setMovies(director.getMovies());
        }

        directorRepository.save(outputDirector);

        return new ResponseEntity<>(outputDirector,HttpStatus.OK);
    }

    /**
     * to delete a director from the db without removing the related values from other tables
     * @param id the id of the to be removed director
     * @return director object of the removed director and ok httpstatus
     */
    public ResponseEntity<Director> deleteDirector(long id){
        // checks if it exists
        if(directorRepository.existsById(id)){
            Director director = directorRepository.findById(id).get();
            //sets the association between director and movies to null (to easily just remove the director)
            director.setMovies(null);
            // remove the director
            directorRepository.delete(director);
            return new ResponseEntity<>(director,HttpStatus.OK);
        }
        return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
    }

}
