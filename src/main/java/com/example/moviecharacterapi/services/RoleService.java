package com.example.moviecharacterapi.services;

import com.example.moviecharacterapi.models.Movie;
import com.example.moviecharacterapi.models.Role;
import com.example.moviecharacterapi.repositories.RoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Set;

/**
 * Service class, to handle the role CRUD operations
 */
public class RoleService {

    // instance of the role repo
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    /**
     * to fetch all the roles from the db
     *
     * @return a list of roles if the list is not empty as well as Ok httpstatus
     * no content and null is returned if the list is empty
     */
    public ResponseEntity<List<Role>> getAllRoles() {
        List<Role> roles = roleRepository.findAll();

        if (roles.size() > 0) {
            return new ResponseEntity<>(roles, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to fetch a specific role from the db
     *
     * @param id the id of the role
     * @return if it exists a role is returned with OK httpstatus
     * if it does not exist, null and no content httpstatus is returned
     */
    public ResponseEntity<Role> getRole(long id) {
        if (roleRepository.existsById(id)) {
            Role role = roleRepository.findById(id).get();
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to add a new role to the db
     *
     * @param role the to be added role
     * @return a role object if it is added successfully, as well as created httpstatus
     * if the user defined the id of the role, then it won't be created and not acceptable is returned
     */
    public ResponseEntity<Role> addRole(Role role) {
        if (role.getRoleId() == 0) {
            Role newRole = roleRepository.save(role);
            return new ResponseEntity<>(newRole, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * to update a specific role
     *
     * @param id         path variable id
     * @param updateRole the updated version of the role
     * @return if it's successful, an updated version of the role is returned as well as Httpstatus ok
     */
    public ResponseEntity<Role> updateRole(long id, Role updateRole) {
        // checks if the role exists
        if (roleRepository.existsById(id)) {
            // if the path variable equal the role id
            if (id == updateRole.getRoleId()) {

                // an instance of the role is fetched
                Role role = roleRepository.findById(id).get();

                // the coming checks is to check which fields are to be updated
                // if the role has null values for some fields, it wont be updated.

                if (updateRole.getName() != null)
                    role.setName(updateRole.getName());

                if (updateRole.getAlias() != null)
                    role.setAlias(updateRole.getAlias());

                if (updateRole.getGender() != null)
                    role.setGender(updateRole.getGender());

                if (updateRole.getPicture() != null)
                    role.setPicture(updateRole.getPicture());

                if (updateRole.getMovies() != null && !updateRole.getMovies().isEmpty()) {
                    role.setMovies(updateRole.getMovies());
                }

                roleRepository.save(role);
                return new ResponseEntity<>(role, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * to delete a role from the db without removing the related values from other tables
     * @param id the id of the to be removed role
     * @return role object of the removed role and ok httpstatus
     */
    public ResponseEntity<Role> deleteRole(long id) {
        // checks if it exists
        if (roleRepository.existsById(id)) {
            Role role = roleRepository.findById(id).get();

            // to remove that specific role from the list of movies
            Set<Movie> movies = roleRepository.findById(id).get().getMovies();
            for (Movie movie : movies
            ) {
                movie.getRoles().remove(role);
            }
            roleRepository.delete(role);
            return new ResponseEntity<>(role, HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
