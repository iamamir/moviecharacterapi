package com.example.moviecharacterapi.controllers;

import com.example.moviecharacterapi.models.Movie;
import com.example.moviecharacterapi.models.Role;
import com.example.moviecharacterapi.repositories.MovieRepository;
import com.example.moviecharacterapi.services.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * rest controller to handle the movie resources and the different http request
 */
@RestController
@RequestMapping("/api/v1/movies")
public class MovieController {

    private final MovieService movieService;

    public MovieController(MovieRepository movieRepository) {
        this.movieService = new MovieService(movieRepository);
    }

    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable long id) {
        return movieService.getMovie(id);
    }

    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        return movieService.addMovie(movie);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable long id, @RequestBody Movie updateMovie) {

        return movieService.updateMovie(id, updateMovie);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Movie> deleteMovie(@PathVariable long id) {
        return movieService.deleteMovie(id);
    }

    @GetMapping("/{id}/roles")
    public ResponseEntity<List<Role>> getRolesByMovie(@PathVariable long id) {
        return movieService.getRolesByMovie(id);
    }
}
