package com.example.moviecharacterapi.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * to display the swagger-ui documentation
 * this should be the first controller to see when first run the application
 */
@Controller
public class HomeController {
    @RequestMapping("/")
    public String greeting() {
        return "redirect:/swagger-ui/index.html";
    }
}