package com.example.moviecharacterapi.controllers;

import com.example.moviecharacterapi.models.Director;
import com.example.moviecharacterapi.repositories.DirectorRepository;
import com.example.moviecharacterapi.services.DirectorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * rest controller to handle the director resources and the different http request
 */
@RestController
@RequestMapping("/api/v1/directors")
public class DirectorController {
    private final DirectorService directorService;

    public DirectorController(DirectorRepository directorRepository){
        this.directorService = new DirectorService(directorRepository);
    }

    @GetMapping()
    public ResponseEntity<List<Director>> getAllDirectors() {
        return directorService.getAllDirectors();
    }

    @PostMapping()
    public ResponseEntity<Director> addDirector(@RequestBody Director director) {
        return directorService.addDirector(director);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Director> getDirector(@PathVariable long id) {
        return directorService.getDirector(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Director> updateDirector(@PathVariable long id, @RequestBody Director director) {
        return directorService.updateDirector(id,director);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Director> deleteDirector(@PathVariable long id) {
        return directorService.deleteDirector(id);
    }
}


