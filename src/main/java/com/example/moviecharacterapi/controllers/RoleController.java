package com.example.moviecharacterapi.controllers;

import com.example.moviecharacterapi.models.Role;
import com.example.moviecharacterapi.repositories.RoleRepository;
import com.example.moviecharacterapi.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * rest controller to handle the role resources and the different http request
 */
@RestController
@RequestMapping("/api/v1/roles")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleRepository roleRepository) {
        roleService = new RoleService(roleRepository);
    }


    @GetMapping()
    public ResponseEntity<List<Role>> getAllRoles() {
        return roleService.getAllRoles();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Role> getRole(@PathVariable long id) {
        return roleService.getRole(id);
    }

    @PostMapping
    public ResponseEntity<Role> addRole(@RequestBody Role role) {
        return roleService.addRole(role);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Role> updateRole(@PathVariable long id, @RequestBody Role updateRole) {
        return roleService.updateRole(id, updateRole);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Role> deleteRole(@PathVariable long id) {
        return roleService.deleteRole(id);
    }
}
