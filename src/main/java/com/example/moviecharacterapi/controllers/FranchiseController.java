package com.example.moviecharacterapi.controllers;

import com.example.moviecharacterapi.models.Franchise;
import com.example.moviecharacterapi.models.Movie;
import com.example.moviecharacterapi.models.Role;
import com.example.moviecharacterapi.repositories.FranchiseRepository;
import com.example.moviecharacterapi.services.FranchiseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Set;
/**
 * rest controller to handle the franchise resources and the different http request
 */
@RestController
@RequestMapping("/api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;

    public FranchiseController(FranchiseRepository franchiseRepository){
        this.franchiseService = new FranchiseService(franchiseRepository);
    }

    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        return franchiseService.getAllFranchises();
    }

    @PostMapping()
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        return franchiseService.addFranchise(franchise);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable long id) {
        return franchiseService.getFranchise(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable long id, @RequestBody Franchise franchise) {
        return franchiseService.updateFranchise(id,franchise);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Franchise> deleteFranchise(@PathVariable long id) {
        return franchiseService.deleteFranchise(id);
    }

    // not standard crud

    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getMoviesInFranchise(@PathVariable long id){
        return franchiseService.getMoviesInFranchise(id);
    }

    @GetMapping("/{id}/roles")
    public ResponseEntity<List<Role>> getRolesInFranchise(@PathVariable long id){

        return franchiseService.getRolesInFranchise(id);
    }



}
