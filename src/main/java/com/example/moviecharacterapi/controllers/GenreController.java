package com.example.moviecharacterapi.controllers;

import com.example.moviecharacterapi.models.Genre;
import com.example.moviecharacterapi.repositories.GenreRepository;
import com.example.moviecharacterapi.services.GenreService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * rest controller to handle the genre resources and the different http request
 */
@RestController
@RequestMapping("/api/v1/genres")
public class GenreController {

    private final GenreService genreService;

    public GenreController(GenreRepository genreRepository){
        this.genreService = new GenreService(genreRepository);
    }

    @GetMapping()
    public ResponseEntity<List<Genre>> getAllGenres(){
        return genreService.getAllGenres();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Genre> getGenre(@PathVariable long id){
        return genreService.getGenre(id);
    }

    @PostMapping
    public ResponseEntity<Genre> addNewGenre(@RequestBody Genre genre){
        return genreService.addGenre(genre);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Genre> updateGenre(@PathVariable long id,@RequestBody Genre genre){
        return genreService.updateGenre(id,genre);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Genre> deleteGenre(@PathVariable long id){
        return genreService.deleteGenre(id);
    }

}
