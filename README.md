# Movie character API 
### Movie character is a restful application. The application is constructed in Spring and uses Spring data and Hibernate to generate the database. The purpose of this API is to provide a datastore to store and manipulate movies. The application uses postgreSQL as the main database, to store and manipulate data about movies, characters and franchises. Furthermore, full CRUD operations are implemented, check the API calls section.
### The code is well organized and commented to explain the functionality as well as api documentation is provided using swagger library. In addition, both the database and the application are deployed and running on Heroku.


# Front end 
* Swagger ui showing the API hierarchy

# Postman Collection Test
* https://www.getpostman.com/collections/657d50bda672ab91d958

# Heroku app from git
* https://movie-character-api-dk.herokuapp.com/

# Api calls : 
# # Director
* Select All Directors
* Select Director {id}
* Insert Director {director Object}
* Update Director {id, directer object}
* Delete Director {id}
# # Frachise
* Select All Frachises
* Select Frachise {id}
* Insert Frachise {franchise Object}
* Update Frachise {id, franchise object}
* Delete Frachise {id}
# # Genre
* Select All Genres
* Select Genre {id}
* Insert Genre {genre Object}
* Update Genre {id, genre object}
* Delete Genre {id}
# # Movie
* Select All Movies
* Select Movie {id}
* Insert Movie {movie Object}
* Update Movie {id, movie object}
* Delete Movie {id}
# # Role
* Select All Roles
* Select Role {id}
* Insert Role {role Object}
* Update Role {id, role object}
* Delete Role {id}
# # Extended
* Select Movies in Franchise {id}
* Select Roles in Movie {id}
* Select Roles in Frachise {id}

# Group
* Amir
* Stephan

